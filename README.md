## Git, Github, Gitlab?  
**Git** est un programme qui permet de suivre les
differentes versions d'un ensemble de fichiers et d'en garder *tout*
l'historique. Il permet aussi de collaborer: pour savoir qui a fait quoi, quand
(et pourquoi) et il va faciliter la fusion de differentes versions d'un projet
sur lequel les gens ont travaillé en parallèle; Git est un outil qui permet à
des centaines de personnes de travailler sur un même projet.

**Github** est un site internet basé sur Git qui s'occupe notamment de
l'interface visuelle entre un utilisateur et Git.

**Gitlab** est aussi un site internet, comme GitHub, mais qui est l'équivalent
libre d'accés de celui-ci. On parle d'instance Gitlab, ainsi le Gitlab du Parti
Pirate est l'une de ces instances.

## Votre premier commit
Dans Git, on parle de **commit** qui sont les étapes
sauvegardées d'un projet. Les changements fait entre les commits peuvent être
facilement perdus alors qu'un commit les inscrits dans le marbre.


1. Vous allez d'abord créer un projet qui contient un fichier.  Pour se faire
il faut d'abord s'inscrire sur le Gitlab.  Ensuite, on vous proposera de créer
un projet ("Create a project").  Si vous avez sauté cette étape vous pourrez
cliquer sur "Projects\>Your projects" et ensuite sur le bouton *New Project*.
Nommez le nouveau projet "MonGit".  Puis cochez la case devant 
**Initialize repository with a README** et enfin
*Create Project*.
2. Maintenant nous allons modifier le fichier README.md, pour cela cliquez sur
README.md puis une nouvelle page s'affiche, vous pourrez alors cliquer sur le
bouton *Edit*. En dessous de:`# MonGit`. Ecrivez: `Mon prmier projet Git` (oui
il y a une faute c'est normal vous allez comprendre pourquoi juste après).
Passez ensuite une ligne et écrivez: `Un second paragraphe`. **Attention il
absolument** laisser une ligne vide entre le premier et le second paragraphe.
Enfin cliquez sur le bouton *Commit changes* pour enregistrer les changements 
on dit aussi **les commiter**.

Et voila! vous avez fait votre premier commit! Si vous faites une fausse
maneouvre lors de ce tuto, vous pourrez à tous instant **supprimer** le projet
en cliquant sur la gauche settings > general, il faudra alors cliquer sur le boutn
*expand* des advanced options puis en descendant tout en bas pour supprimer le
projet en suivant les intructions. Ainsi vous pourrez recommencer le tuto sur une
base fraîche.

## Simuler la collaboration entre trois personnes
Dans cette partie nous allons
vous initier au travail en parallèle et à la fusion de ces travaux. On simule
ici une collaboration entre trois personnes:
1. L'actrice principale de cette histoire: **Charlie**
1. Une qui corrige les fautes: **Bob**
1. Une autres qui corrige les fautes: **Alice**

Dans ce scénario c'est **Charlie** qui va coordoner la fusion des différentes
branches.

### Créer une branche et la modifier
On se place ici à la place de **Bob**. Sur la gauche de l'écran vous allez 
pouvoir cliquer sur *Project overview*, vous allez alors avoir une vision 
globale du projet.  Cliquez alors sur le petit + et ensuite sur *New branch* 
dans le menu déroulant.
1. On va nommer la nouvelle branche *bob*.
2. Cette branche est la copie de la branche principale C'est sur cette branche
que l'on va pouvoir travailler pour corriger la faute de frappe. On est
automatiquement placé sur cette branche.  Corrigeons alors la faute en cliquant
sur README.md puis sur *Edit*  et après corrections on pourra cliquer sur
*commit changes*.

### Continuer sur la branche principale pendant ce temps
Ici on incarne
**Charlie** qui va continuer à éditer sa version.
1. Dans le menu déroulant sur la gauche sélectionnez maintenant la branche
principale (*master* ou *main*) qui est la branche de Charlie
2. On va modifier le second paragraphe du fichier README.md en écrivant `Un
second paragraphe qui explque tout` (Oui il y a une autre typo c'est normal
Charlie a du mal.  enfin on commit ce changement en cliquant sur *commit
change*.

Maintenant on peut fusionner le tout 

### Fusionner les branches de **Bob** et **Charlie** On procède en deux étapes: 
1. **Bob** demande de fusionner deux branches du projet (merge request en
anglais).
2. **Charlie** traite cette demande.

C'est d'abord **Bob** qui va faire chauffer sa souris:
1. Clic sur **Merge requests** à gauche de votre écran.
2. Clic sur le bouton **New merge request**.
3. Selectionnez la branche *bob* dans le menu déroulant *Select source
branch* et c'est la branche principale qui va être notre target branch
par défaut.
4. Clic sur *Submit merge request* une nouvelle page apparaît alors. 

Puis on va se mettre dans les baskets de **Charlie** et cliquer finalement sur
le bouton vert *Merge* lorsque la page apparaît.

Vous avez maintenant un projet Git corrigé avec de nouveau une seule branche.
Ce projet est le fruit de la collaboration entre **Bob** et **Charlie**.

### Créer une autre branche et la modifier
On se place ici dans la peau d'**Alice**. Créez maintenant une branche *typo*
en utilisant la même technique que pour créer la branche *alice*.

Sur cette branche on va modifier *README.md* en cliquant sur README.md puis
*Edit* et on va corriger la coquille du second paragraphe. Après cette 
correction elle pourra cliquer sur *commit changes*.

### Continuer sur la branche principale pendant ce temps 
On incarne de nouveau **Charlie** qui va continuer à éditer
sa version pendant ce temps.
On selectionne de nouveau la branche *master* dans le menu déroulant à guache
et on va modifier le second paragraphe du fichier README.md en écrivant 
`Un second paragraphe qui dis tout`enfin on commit se changement.

Maintenant vient le temps d'un choix. 

### Régler le conflit entre **Alice** et **Charlie**
On recréer une demande de fusion, normalement c'est **Alice** qui
émet cette demande, c'est maintenant à **Alice** de faire plein de clics.
La branche source sera alors *alice* et la target branch sera 
toujours *master*.

A la dernière étape on reincarne *Charlie* qui doit fusionner les deux branches
maintenant il se retrouve devanr un choix.
Le merge ne peut pas être automatique et on nous demande de choisir 
après avoir cliquer sur *resolve conflict* on va devoir choisir entre
'explique' et 'dis' car **Alice** et **Charlie** ont modifiés la même ligne d'un
fichier chacun de leurs côté. **Charlie** clic alors sur *Our* ou *Their* selon
le changement que vous préfèré et il peut ensuite fusionner le tout en cliquant
sur le bouton *merge*.

Et voila ! Vous avez maintenant le produit final issu du travail conjoint
de **Bob**, **Alice** et **Charlie**. Il devrait contenir un fichier 
README.md où il est écrit:

    #MonGit
    Mon premier dépôt Git
    
    Un autre pargraphe qui explique tout
    
Si **Charlie** a suivi les corrections de **Alice**.

### Un tutoriel interactif
Ce principe de branchage et de merge request fonctionne de la même façon quand 
on travail au sein d'un grand groupe de personnes. Ce guide est interactif et 
n'importe quel membre de l'équipage Tuto pourra jouer le rôle de **Charlie**:
Il vous suffit de suivre ces trois étapes:
1. Demander à collaborer sur [ce projet](https://framagit.org/PersonnePirate/gitlab) 
en cliquant sur "Request access" 
2. Créer une nouvelle branche et faire vos modifications dessus
3. Emettre une merge request

A vos souris et clavier pour améliorer ce tuto :smile:. Pour apprendre à faire rapidement des beaux
README.md il y a un guide [Markdown](https://framagit.org/RomainC/Markdown)
. Et si une merge request (MR pour les intimes) vous semble trop ou pas 
assez vous pouvez ouvrir une [*issue*](https://framagit.org/RomainC/gitlab/-/issues) 
pour partager vos impressions sur ce guide.

